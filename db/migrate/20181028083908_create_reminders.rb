class CreateReminders < ActiveRecord::Migration[5.2]
  def change
    create_table :reminders, id: :uuid do |t|
      t.uuid :user_id, null: false, foreign_key: true

      t.string :message, null: false
      t.timestamp :notify_at, null: false
      t.timestamp :processed_at, default: nil

      t.timestamps
    end
  end
end
