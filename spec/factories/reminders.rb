FactoryBot.define do
  factory :reminder do
    association :user, factory: :user, strategy: :create
    message { Faker::Lorem.sentence }
    notify_at { Faker::Date.between(Date.today, 5.years.from_now) }
  end
end
