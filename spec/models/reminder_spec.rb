require 'rails_helper'

RSpec.describe Reminder, type: :model do
  it 'has a valid factory' do
    expect(FactoryBot.build(:reminder).save).to be true
  end

  it 'must be associated with a user' do
    expect(FactoryBot.build(:reminder, user: nil).save).to be false
  end

  it 'must have a message' do
    expect(FactoryBot.build(:reminder, message: nil).save).to be false
  end

  it 'must have a notify date' do
    expect(FactoryBot.build(:reminder, notify_at: nil).save).to be false
  end
end
