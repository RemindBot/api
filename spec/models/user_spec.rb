require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has a valid factory' do
    expect(FactoryBot.build(:user).save).to be true
  end

  it 'must have a username' do
    expect(FactoryBot.build(:user, username: nil).save).to be false
  end
end
