class Reminder < ApplicationRecord
  validates_presence_of :user_id, :message, :notify_at

  belongs_to :user
end
